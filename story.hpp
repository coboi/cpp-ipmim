namespace story {
	void setStory(){
		if(var::meter == 0){
			std::cout << " Di sebuah malam pada Desember 1999," << std::endl;
			std::cout << " kamu sedang bermimpi di dalam tidurmu." << std::endl;
			std::cout << " Saat itu kamu masih berumur tiga tahun." << std::endl;
			std::cout << " Tepat pukul 21:02 kamu mulai bermimpi" << std::endl;
			std::cout << " tentang dirimu yang tiba-tiba menjadi" << std::endl;
			std::cout << " seorang laki-laki yang kemungkinan" << std::endl;
			std::cout << " berumur sekitar enam tahun. Kamu berada" << std::endl;
			std::cout << " pada ujung ruangan yang sepertinya" << std::endl;
			std::cout << " sangat panjang, dan kamu mencoba" << std::endl;
			std::cout << " berjalan ke sisi ujung yang lain." << std::endl << std::endl;
			
			std::cout << " Ketik lanjut untuk berjalan" << std::endl;
			var::meter += 1;
		} else if(var::meter == 9){
			std::cout << " Kamu melihat jam bertuliskan 5:49" << std::endl << std::endl;
			std::cout << " ---------------------------------------------" << std::endl << std::endl;

			std::cout << " Kamu membuka matamu, dan seperti" << std::endl;
			std::cout << " biasanya kamu melihat jam di samping" << std::endl;
			std::cout << " kanan tempat tidurmu. Kamu melihat" << std::endl;
			std::cout << " sekilas jam itu bertuliskan 5:49 dan" << std::endl;
			std::cout << " kamu bersiap-siap untuk berangkat ke" << std::endl;
			std::cout << " tempat ujian kelulusan. Kamu berjalan" << std::endl;
			std::cout << " keluar rumah sambil terus memikirkan" << std::endl;
			std::cout << " ujian kelulusan nanti. Saat sampai" << std::endl;
			std::cout << " di luar rumah kamu bingung, kenapa" << std::endl;
			std::cout << " mataharinya cukup tinggi di pagi hari" << std::endl;
			std::cout << " seperti ini. Kamu berjalan ke tempat" << std::endl;
			std::cout << " ujian dengan membawa tanda tanya yang" << std::endl;
			std::cout << " belum terjawab itu. Belum lama berjalan" << std::endl;
			std::cout << " menjauh dari rumahmu, kamu melihat" << std::endl;
			std::cout << " toko yang sudah buka, yang biasanya" << std::endl;
			std::cout << " masih tutup setiap kali kamu melewatinya" << std::endl;
			std::cout << " di pagi hari. Kamu melihat toko itu" << std::endl;
			std::cout << " sambil terus berjalan dan ternyata" << std::endl;
			std::cout << " disana ada sebuah jam digital yang" << std::endl;
			std::cout << " bisa dilihat dengan baik dari kejauhan." << std::endl;
			std::cout << " Disana tertulis 9:02, kamu berpikir" << std::endl;
			std::cout << " apa mungkin jam di kamar tadi mati." << std::endl;
			std::cout << " 'Sial', kamu panik dan langsung berlari" << std::endl;
			std::cout << " sekuat tenaga menuju tempat ujian." << std::endl;
			std::cout << " Kamu memaksa tubuhmu yang sebenarnya" << std::endl;
			std::cout << " sudah cukup lama tidak digunakan" << std::endl;
			std::cout << " untuk lari itu. Tiba-tiba kamu tidak" << std::endl;
			std::cout << " merasakan apa pun, yang kamu lihat" << std::endl;
			std::cout << " hanyalah bintang-bintang yang bersinar" << std::endl;
			std::cout << " seperti di malam hari." << std::endl;
			var::meter += 1;
		} else if(var::meter == 21){
			std::cout << " Kamu melihat pita kupu-kupu" << std::endl << std::endl;
			std::cout << " ---------------------------------------------" << std::endl << std::endl;

			std::cout << " 'Seminggu lagi ya ternyata' pikirmu" << std::endl;
			std::cout << " dalam hati. Sore itu kamu sedang berjalan" << std::endl;
			std::cout << " pulang dengan pikiran yang penuh." << std::endl;
			std::cout << " Tanpa sadar, ternyata di sampingmu ada" << std::endl;
			std::cout << " seorang perempuan yang sedang berjalan" << std::endl;
			std::cout << " juga. Dia memakai baju dengan tulisan" << std::endl;
			std::cout << " yang sama dengan baju yang kamu pakai." << std::endl;
			std::cout << " Dia juga memakai pita kupu-kupu berwarna" << std::endl;
			std::cout << " hujau cerah untuk mengikat rambutnya" << std::endl;
			std::cout << " yang panjang itu. Kamu berpikir mungkin" << std::endl;
			std::cout << " dia juga baru saja pulang. Kamu memandangi" << std::endl;
			std::cout << " dirinya cukup lama dan berpikir untuk" << std::endl;
			std::cout << " mencoba mengajaknya berbicara. Tetapi," << std::endl;
			std::cout << " sebelum kamu mengeluarkan satu kata pun," << std::endl;
			std::cout << " ternyata dia sudah berbelok saat" << std::endl;
			std::cout << " persimpangan. 'Mungkin saja rumahnya" << std::endl;
			std::cout << " lewat jalan itu' pikirmu. Kamu tetap " << std::endl;
			std::cout << " terus berjalan lurus, saat pikiranmu" << std::endl;
			std::cout << " sudah penuh, ternyata ada sesuatu yang" << std::endl;
			std::cout << " memaksa masuk hingga bisa dibilang" << std::endl;
			std::cout << " penuh sesak." << std::endl;
			var::meter += 1;
		} else if(var::meter == 34){
			std::cout << " Kamu melihat buku" << std::endl << std::endl;
			std::cout << " ---------------------------------------------" << std::endl << std::endl;

			std::cout << " Hari itu ternyata kamu datang lebih" << std::endl;
			std::cout << " awal dari biasanya. Karena belum banyak" << std::endl;
			std::cout << " orang, kamu membuka sebuah buku dan" << std::endl;
			std::cout << " berkata dalam hati 'sebentar lagi ujian" << std::endl;
			std::cout << " kelulusan, semoga saja lulus'. Hampir" << std::endl;
			std::cout << " tigapuluh menit kamu membaca buku" << std::endl;
			std::cout << " tersebut, tanpa sadar ternyata sudah" << std::endl;
			std::cout << " cukup banyak orang yang datang. Ada" << std::endl;
			std::cout << " seseorang yang datang mendekatimu, dia" << std::endl;
			std::cout << " mengajak dirimu ke suatu tempat setelah" << std::endl;
			std::cout << " pulang nanti. 'Oke' balasmu, tanpa" << std::endl;
			std::cout << " pikir panjang kamu mengiyakan ajakan" << std::endl;
			std::cout << " tersebut. Kamu berpikir, karena sudah" << std::endl;
			std::cout << " lama kamu tidak kesana lagi dengan" << std::endl;
			std::cout << " seseorang yang sudah kamu kenal selama" << std::endl;
			std::cout << " dua tahun lebih itu. Kemudian terdengar" << std::endl;
			std::cout << " suara bel, dan dia berkata 'jumpa" << std::endl;
			std::cout << " lagi nanti sore ya'." << std::endl;
			var::meter += 1;
		} else if(var::meter == 40){
			std::cout << " Kamu melihat mainan berbentuk buaya" << std::endl << std::endl;
			std::cout << " ---------------------------------------------" << std::endl << std::endl;

			std::cout << " Kamu sedang duduk di kursi di dalam" << std::endl;
			std::cout << " sebuah ruangan. Kamu melihat ke depan dengan" << std::endl;
			std::cout << " semangat. Di depan sana terlihat seseorang" << std::endl;
			std::cout << " yang jauh lebih tua dari dirimu sedang" << std::endl;
			std::cout << " berbicara. Ternyata dia sedang bercerita" << std::endl;
			std::cout << " tentang 'kancil dan buaya'. Kamu sangat" << std::endl;
			std::cout << " bergembira saat menyimak cerita tersebut," << std::endl;
			std::cout << " begitu pula dengan orang-orang di kanan," << std::endl;
			std::cout << " kiri dan di belakangmu. Sepuluh menit" << std::endl;
			std::cout << " kemudian, orang yang ada didepan sudah" << std::endl;
			std::cout << " pergi meninggalkan ruangan. Kamu mengajak" << std::endl;
			std::cout << " orang di samping kananmu untuk berbicara." << std::endl;
			std::cout << " Kalian membahas tentang cerita yang" << std::endl;
			std::cout << " tadi, dan kalian tertawa bersama." << std::endl;
			var::meter += 1;
		} else if(var::meter == 49){
			std::cout << " Kamu tidak merasakan apapun, kamu tidak" << std::endl;
			std::cout << " bisa melakukan apapun. Bintang-bintang" << std::endl;
			std::cout << " yang kamu lihat itu, terlihat seperti" << std::endl;
			std::cout << " kasihan kepadamu. Bagaimana tidak, selama" << std::endl;
			std::cout << " limabelas tahun hidupmu itu, kamu melewati" << std::endl;
			std::cout << " setiap harinya dengan normal-normal saja," << std::endl;
			std::cout << " walaupun tidak selalu berjalan baik. Tapi" << std::endl;
			std::cout << " hari itu, kamu seperti sial sepanjang" << std::endl;
			std::cout << " hari dan bahkan berakhir tidak tahu" << std::endl;
			std::cout << " dimana kamu berada. Tiba-tiba kamu" << std::endl;
			std::cout << " terbangun di kamarmu. Ternyata di" << std::endl;
			std::cout << " kamarmu ada seseorang, dia sadar bahwa" << std::endl;
			std::cout << " kamu sudah bangun. Dia mendekatimu" << std::endl;
			std::cout << " dengan wajah sangat bahagia, tapi tidak" << std::endl;
			std::cout << " denganmu. Wajahmu menunjukkan ekspresi" << std::endl;
			std::cout << " aneh, bahkan sangat aneh untuk anak" << std::endl;
			std::cout << " berumur tiga tahun." << std::endl << std::endl;

			std::cout << " ---------------------------------------------" << std::endl << std::endl;
			std::cout << " Ipmim - 2019" << std::endl;
			std::cout << " coboi c0boi c0b0i" << std::endl << std::endl;
			std::cout << " ---------------------------------------------" << std::endl << std::endl;
			std::cout << " Terima kasih :)" << std::endl;

			var::meter = 0;
			var::enemyTurn = 0;
			var::isUpgraded = "n";
		}
	}
}