namespace qn {
	void questionData(){
		if(var::qn == 1){
			var::answer = "a";

			std::cout << " 4 + 2 = ?" << std::endl;
			std::cout << " a. 6" << std::endl;
			std::cout << " b. 9" << std::endl;
			std::cout << " c. 5" << std::endl;
		} else if(var::qn == 2){
			var::answer = "c";

			std::cout << " Meong meong itu suaranya..." << std::endl;
			std::cout << " a. Anjing" << std::endl;
			std::cout << " b. Singa" << std::endl;
			std::cout << " c. Kucing" << std::endl;
		} else if(var::qn == 3){
			var::answer = "a";

			std::cout << " Mata kita ada..." << std::endl;
			std::cout << " a. Dua" << std::endl;
			std::cout << " b. Empat" << std::endl;
			std::cout << " c. Enam" << std::endl;
		} else if(var::qn == 4){
			var::answer = "a";

			std::cout << " 123 * 4 + 5 = ?" << std::endl;
			std::cout << " a. 497" << std::endl;
			std::cout << " b. 1107" << std::endl;
			std::cout << " c. 12345" << std::endl;
		} else if(var::qn == 5){
			var::answer = "a";

			std::cout << " Cerita komedi adalah cerita yang" << std::endl;
			std::cout << " bisa membuat kita..." << std::endl;
			std::cout << " a. Tertawa" << std::endl;
			std::cout << " b. Menangis" << std::endl;
			std::cout << " c. Sedih" << std::endl;
		} else if(var::qn == 6){
			var::answer = "b";

			std::cout << " Kincir air dapat berputar karena..." << std::endl;
			std::cout << " a. Panas matahari" << std::endl;
			std::cout << " b. Air mengalir" << std::endl;
			std::cout << " c. Gerak angin" << std::endl;
		} else if(var::qn == 7){
			var::answer = "b";

			std::cout << " 2^9 = ?" << std::endl;
			std::cout << " a. 18" << std::endl;
			std::cout << " b. 512" << std::endl;
			std::cout << " c. 256" << std::endl;
		} else if(var::qn == 8){
			var::answer = "c";

			std::cout << " Pesan yang dapat diambil dari" << std::endl;
			std::cout << " sebuah cerita disebut..." << std::endl;
			std::cout << " a. Alur" << std::endl;
			std::cout << " b. Watak" << std::endl;
			std::cout << " c. Amanat" << std::endl;
		} else if(var::qn == 9){
			var::answer = "a";

			std::cout << " Berikut sumber-sumber cahaya," << std::endl;
			std::cout << " kecuali..." << std::endl;
			std::cout << " a. Kayu" << std::endl;
			std::cout << " b. Matahari" << std::endl;
			std::cout << " c. Lilin" << std::endl;
		} else if(var::qn == 10){
			var::answer = "b";

			std::cout << " Diketahui segitiga ABC adalah" << std::endl;
			std::cout << " segitiga siku-siku, dengan siku-siku" << std::endl;
			std::cout << " di B, jika panjang AB = 6 dan BC = 8," << std::endl;
			std::cout << " maka panjang AC = ?" << std::endl;
			std::cout << " a. 11" << std::endl;
			std::cout << " b. 10" << std::endl;
			std::cout << " c. 13" << std::endl;
		} else if(var::qn == 11){
			var::answer = "a";

			std::cout << " Yang termasuk ciri-ciri pantun" << std::endl;
			std::cout << " adalah..." << std::endl;
			std::cout << " a. Terdiri dari 4 baris" << std::endl;
			std::cout << " b. Terdiri dari 5 baris" << std::endl;
			std::cout << " c. Baris pertama merupakan isi" << std::endl;
		} else if(var::qn == 12){
			var::answer = "a";

			std::cout << " Satuan Internasional dari panjang" << std::endl;
			std::cout << " adalah..." << std::endl;
			std::cout << " a. meter" << std::endl;
			std::cout << " b. kilogram" << std::endl;
			std::cout << " c. sekon" << std::endl;
		} else if(var::qn == 13){
			var::answer = "b";

			std::cout << " Diketahui sebuah balok ABCD.EFGH" << std::endl;
			std::cout << " dengan panjang AB = 10, BC = 6 dan" << std::endl;
			std::cout << " CG = 12, maka volume balok tersebut" << std::endl;
			std::cout << " adalah..." << std::endl;
			std::cout << " a. 620" << std::endl;
			std::cout << " b. 720" << std::endl;
			std::cout << " c. 10612" << std::endl;
		} else if(var::qn == 14){
			var::answer = "a";

			std::cout << " Berikut ciri-ciri novel, kecuali..." << std::endl;
			std::cout << " a. Maksimal 10000 kata" << std::endl;
			std::cout << " b. Biasanya lebih dari 35000 kata" << std::endl;
			std::cout << " c. Memiliki alur yang kompleks" << std::endl;
		} else if(var::qn == 15){
			var::answer = "b";

			std::cout << " Diketahui suatu benda diukur dengan" << std::endl;
			std::cout << " termometer Fahrenheit menunjukkan suhu" << std::endl;
			std::cout << " 77 Derajat Fahrenheit, jika benda tersebut" << std::endl;
			std::cout << " diukur dengan termometer Celsius, maka" << std::endl;
			std::cout << " akan menunjukkan suhu ... Derajat Celsius" << std::endl;
			std::cout << " a. 45" << std::endl;
			std::cout << " b. 25" << std::endl;
			std::cout << " c. 15" << std::endl;
		} else if(var::qn == 16){
			var::answer = "a";

			std::cout << " Diketahui x + 2y = 17 dan 2x + y = 16," << std::endl;
			std::cout << " jadi x + y = ?" << std::endl;
			std::cout << " a. 11" << std::endl;
			std::cout << " b. 12" << std::endl;
			std::cout << " c. 13" << std::endl;
		}
	}

	void setQuestion(){
		if(var::meter == 5){
			var::qn = 1;
		} else if(var::meter == 7){
			var::qn = 2;
		} else if(var::meter == 8){
			var::qn = 3;
		} else if(var::meter == 11){
			var::qn = 4;
		} else if(var::meter == 12){
			var::qn = 5;
		} else if(var::meter == 14){
			var::qn = 6;
		} else if(var::meter == 18){
			var::qn = 7;
		} else if(var::meter == 22){
			var::qn = 8;
		} else if(var::meter == 24){
			var::qn = 9;
		} else if(var::meter == 27){
			var::qn = 10;
		} else if(var::meter == 28){
			var::qn = 11;
		} else if(var::meter == 29){
			var::qn = 12;
		} else if(var::meter == 32){
			var::qn = 13;
		} else if(var::meter == 42){
			var::qn = 14;
		} else if(var::meter == 44){
			var::qn = 15;
		} else if(var::meter == 46){
			var::qn = 16;
		}
	}

	void choiceRight(){
		std::cout << "\n Yuhuu kamu benar" << std::endl;
		var::chance = 2;
		var::meter += 1;
	}

	void choiceWrong(){
		var::chance -= 1;
		if(var::chance >= 1){
			std::cout << "\n Hmm kamu salah" << std::endl;
			std::cout << " Kesempatanmu tinggal " << var::chance << std::endl << std::endl;
		} else if(var::chance <= 0){
			std::cout << "\n Yaaah kesempatanmu habis :(" << std::endl;
			var::meter = 0;
			var::isUpgraded = "n";
		}
	}

	void questionTime(){
		while(true){
			setQuestion();
			questionData();
			std::cout << " >";
			std::cin >> var::cmdQuestion;
			if((var::cmdQuestion == "a") || (var::cmdQuestion == "b") || (var::cmdQuestion == "c")){
				if(var::cmdQuestion == var::answer){
					choiceRight();
					break;
				} else{
					choiceWrong();
					if(var::chance <= 0){
						break;
					}
				}
			} else{
				std::cout << "\n Tinggal milih aja gak bisa" << std::endl << std::endl;
			}
		}
	}
}