namespace upgrade {
	void setUpgradeStat(){
		if(var::upgradeStat == 1){
			battle::Player.setName(battle::Player.getName());
			battle::Player.setPower(battle::Player.getPower());
			battle::Player.setHp(var::tempPlayerMaxHp + 50);
		} else if(var::upgradeStat == 2){
			battle::Player.setName(battle::Player.getName());
			battle::Player.setPower(battle::Player.getPower() + 10);
			battle::Player.setHp(var::tempPlayerMaxHp);
		}

		battle::setBattle();
	}

	void selectUpgrade(){
		std::cout << " Kamu dapet bonus status" << std::endl << std::endl;
		std::cout << " Pilih bonus status" << std::endl;
		std::cout << " a. +50 HP, atau" << std::endl;
		std::cout << " b. +10 AP" << std::endl;
		std::cout << " >";
		std::cin >> var::cmdUpgrade;
		if(var::cmdUpgrade == "a"){
			var::upgradeStat = 1;
			var::isUpgraded = "y";
			std::cout << "\n Kamu dapet +50 HP" << std::endl;
			setUpgradeStat();
			var::meter += 1;
		} else if(var::cmdUpgrade == "b"){
			var::upgradeStat = 2;
			var::isUpgraded = "y";
			std::cout << "\n Kamu dapet +10 AP" << std::endl;
			setUpgradeStat();
			var::meter += 1;
		} else{
			std::cout << "\n Bisa milih gak sih" << std::endl;
			std::cout << " Ulangi milih yang bener" << std::endl;
		}
		var::tempPlayerMaxHp = battle::Player.getHp();
	}
}