namespace var {
	// global
	std::string cmd;
	std::string cmdBattle;
	std::string cmdQuestion;
	std::string cmdUpgrade;
	std::string tempPlayerName;
	std::fstream myData;
	int meter = 0;
	int tempPlayerPower;

	// question
	std::string answer;
	int qn = 0;
	int chance = 2;

	// battle
	int enemyBattle = 0;
	int tempPlayerMaxHp;
	int tempEnemyMaxHp;
	int tempPlayerHp;
	int tempEnemyHp;
	int enemyTurn = 0;

	// upgrade
	std::string isUpgraded = "n";
	int upgradeStat;
}