namespace battle{
	class Chara{
	protected:
		std::string name_;
		int power_;
		int hp_;
	public:
		virtual std::string getName() = 0;
		virtual int getPower() = 0;
		virtual int getHp() = 0;
		virtual void setName(std::string name) = 0;
		virtual void setPower(int power) = 0;
		virtual void setHp(int hp) = 0;
	};

	class PlayerChara:public Chara{
	public:
		std::string getName(){
			return name_;
		}
		int getPower(){
			return power_;
		}
		int getHp(){
			return hp_;
		}
		void setName(std::string name){
			name_ = name;
		}
		void setPower(int power){
			power_ = power;
		}
		void setHp(int hp){
			hp_ = hp;
		}
	};

	class EnemyChara:public Chara{
	public:
		std::string getName(){
			return name_;
		}
		int getPower(){
			return power_;
		}
		int getHp(){
			return hp_;
		}
		void setName(std::string name){
			name_ = name;
		}
		void setPower(int power){
			power_ = power;
		}
		void setHp(int hp){
			hp_ = hp;
		}
	};

	PlayerChara Player;
	EnemyChara Enemy;

	void setPlayerDefaultStat(){
		Player.setName("Kamu");
		Player.setPower(20);
		Player.setHp(50);
	}

	void setPlayerUpgradedStat(){
		Player.setName(Player.getName());
		Player.setPower(Player.getPower());
		Player.setHp(var::tempPlayerMaxHp);
	}

	void setBattle(){
		if(var::enemyBattle == 1){
			Enemy.setName(enemy::A.name);
			Enemy.setPower(enemy::A.power);
			Enemy.setHp(enemy::A.hp);
		} else if(var::enemyBattle == 2){
			Enemy.setName(enemy::B.name);
			Enemy.setPower(enemy::B.power);
			Enemy.setHp(enemy::B.hp);
		} else if(var::enemyBattle == 3){
			Enemy.setName(enemy::C.name);
			Enemy.setPower(enemy::C.power);
			Enemy.setHp(enemy::C.hp);
		} else if(var::enemyBattle == 4){
			Enemy.setName(enemy::D.name);
			Enemy.setPower(enemy::D.power);
			Enemy.setHp(enemy::D.hp);
		} else if(var::enemyBattle == 5){
			Enemy.setName(enemy::E.name);
			Enemy.setPower(enemy::E.power);
			Enemy.setHp(enemy::E.hp);
		} else if(var::enemyBattle == 6){
			Enemy.setName(enemy::F.name);
			Enemy.setPower(enemy::F.power);
			Enemy.setHp(enemy::F.hp);
		} else if(var::enemyBattle == 7){
			Enemy.setName(enemy::G.name);
			Enemy.setPower(enemy::G.power);
			Enemy.setHp(enemy::G.hp);
		} else if(var::enemyBattle == 8){
			Enemy.setName(enemy::H.name);
			Enemy.setPower(enemy::H.power);
			Enemy.setHp(enemy::H.hp);
		} else if(var::enemyBattle == 9){
			Enemy.setName(enemy::I.name);
			Enemy.setPower(enemy::I.power);
			Enemy.setHp(enemy::I.hp);
		} else if(var::enemyBattle == 10){
			Enemy.setName(enemy::J.name);
			Enemy.setPower(enemy::J.power);
			Enemy.setHp(enemy::J.hp);
		}
	}

	void setEnemy(){
		if(var::meter == 3){
			var::enemyBattle = 1;
		} else if(var::meter == 6){
			var::enemyBattle = 2;
		} else if(var::meter == 13){
			var::enemyBattle = 3;
		} else if(var::meter == 19){
			var::enemyBattle = 4;
		} else if(var::meter == 20){
			var::enemyBattle = 5;
		} else if(var::meter == 26){
			var::enemyBattle = 6;
		} else if(var::meter == 35){
			var::enemyBattle = 7;
		} else if(var::meter == 41){
			var::enemyBattle = 8;
		} else if(var::meter == 45){
			var::enemyBattle = 9;
		} else if(var::meter == 48){
			var::enemyBattle = 10;
		}
	}

	void defaultStat(){
		enemy::enemyData();
		setPlayerDefaultStat();
	}

	void upgradedStat(){
		enemy::enemyData();
		setPlayerUpgradedStat();
	}

	void statBattle(){
		std::cout << " " << Player.getName() << " - HP " << Player.getHp() << "/" << var::tempPlayerMaxHp << " - AP " << Player.getPower() << std::endl;
		std::cout << " " << Enemy.getName() << " - HP " << Enemy.getHp() << "/" << var::tempEnemyMaxHp << " - AP " << Enemy.getPower() << std::endl;
	}

	void isKo(){
		if(Player.getHp() <= 0){
			Player.setHp(0);
		}
		if(Enemy.getHp() <= 0){
			Enemy.setHp(0);
		}
	}

	void battleDraw(){
		std::cout << "\n Kalian imbang, sama sama mati, jadi" << std::endl;
		std::cout << " sama aja kamu kalah wkwkwkwk" << std::endl;
		var::meter = 0;
		var::enemyTurn = 0;
		var::isUpgraded = "n";
	}
	void battleWin(){
		std::cout << "\n Yayyyyyy kamu menang" << std::endl;
		var::meter += 1;
		var::enemyTurn = 0;
	}
	void battleLose(){
		std::cout << "\n Yaaah kamu kalah :(" << std::endl;
		var::meter = 0;
		var::enemyTurn = 0;
		var::isUpgraded = "n";
	}

	void battleAttAtt(){
		std::cout << "\n " << Player.getName() << " menyerang " << Enemy.getName() << std::endl;
		std::cout << " " << Enemy.getName() << " menyerang " << Player.getName() << std::endl << std::endl;
		var::tempPlayerHp = (Player.getHp() - Enemy.getPower());
		var::tempEnemyHp = (Enemy.getHp() - Player.getPower());

		Player.setHp(var::tempPlayerHp);
		Enemy.setHp(var::tempEnemyHp);

		isKo();

		statBattle();
		var::enemyTurn += 1;
	}

	void battleAttDef(){
		std::cout << "\n " << Player.getName() << " menyerang " << Enemy.getName() << std::endl;
		std::cout << " " << Enemy.getName() << " bertahan dari " << Player.getName() << std::endl << std::endl;
		var::tempEnemyHp = (Enemy.getHp() - (Player.getPower() / 2));

		Enemy.setHp(var::tempEnemyHp);

		isKo();

		statBattle();
		var::enemyTurn += 1;
	}

	void battleDefAtt(){
		std::cout << "\n " << Player.getName() << " bertahan dari " << Enemy.getName() << std::endl;
		std::cout << " " << Enemy.getName() << " menyerang " << Player.getName() << std::endl << std::endl;
		var::tempEnemyHp = (Enemy.getHp() - (Player.getPower() / 2));
		var::tempPlayerHp = (Player.getHp() - (Enemy.getPower() / 2));

		Player.setHp(var::tempPlayerHp);

		isKo();

		statBattle();
		var::enemyTurn += 1;
	}

	void battleDefDef(){
		std::cout << "\n " << Player.getName() << " bertahan dari " << Enemy.getName() << std::endl;
		std::cout << " " << Enemy.getName() << " bertahan dari " << Player.getName() << std::endl << std::endl;
		var::tempPlayerHp = Player.getHp();
		var::tempEnemyHp = Enemy.getHp();

		Player.setHp(var::tempPlayerHp);
		Enemy.setHp(var::tempEnemyHp);

		isKo();

		statBattle();
		var::enemyTurn += 1;
	}

	void startBattle(){
		setEnemy();
		setBattle();
		var::tempPlayerMaxHp = Player.getHp();
		var::tempEnemyMaxHp = Enemy.getHp();
		std::cout << " Kamu ketemu musuh -- " << Enemy.getName() << std::endl;
		statBattle();
		while(true){
			std::cout << "\n Pilih aksimu" << std::endl;
			std::cout << " a. Serang musuh" << std::endl;
			std::cout << " b. Bertahan dari musuh" << std::endl;
			std::cout << " >";
			std::cin >> var::cmdBattle;
			if(var::cmdBattle == "a"){
				if(var::enemyTurn % 2 == 0){
					battleAttAtt();
					if((Enemy.getHp() <= 0) && (Player.getHp() <= 0)){
						battleDraw();
						break;
					} else if(Enemy.getHp() <= 0){
						battleWin();
						break;
					} else if(Player.getHp() <= 0){
						battleLose();
						break;
					}
				} else if(var::enemyTurn % 2 == 1){
					battleAttDef();
					if((Enemy.getHp() <= 0) && (Player.getHp() <= 0)){
						battleDraw();
						break;
					} else if(Enemy.getHp() <= 0){
						battleWin();
						break;
					} else if(Player.getHp() <= 0){
						battleLose();
						break;
					}
				}
			} else if(var::cmdBattle == "b"){
				if(var::enemyTurn % 2 == 0){
					battleDefAtt();
					if((Enemy.getHp() <= 0) && (Player.getHp() <= 0)){
						battleDraw();
						break;
					} else if(Enemy.getHp() <= 0){
						battleWin();
						break;
					} else if(Player.getHp() <= 0){
						battleLose();
						break;
					}
				} else if(var::enemyTurn % 2 == 1){
					battleDefDef();
					if((Enemy.getHp() <= 0) && (Player.getHp() <= 0)){
						battleDraw();
						break;
					} else if(Enemy.getHp() <= 0){
						battleWin();
						break;
					} else if(Player.getHp() <= 0){
						battleLose();
						break;
					}
				}
			} else{
				std::cout << "\n Bisa gelut gak sih, cupu" << std::endl;
			}
		}
	}
}