namespace enemy{
	struct enemy{
		std::string name;
		int power;
		int hp;
	};

	enemy A;
	enemy B;
	enemy C;
	enemy D;
	enemy E;
	enemy F;
	enemy G;
	enemy H;
	enemy I;
	enemy J;

	void enemyData(){
		A.name = "Gatimn";
		A.power = 10;
		A.hp = 50;

		B.name = "Alonu";
		B.power = 15;
		B.hp = 50;

		C.name = "Jajaiy";
		C.power = 15;
		C.hp = 75;

		// upgrade 1
		D.name = "Orego";
		D.power = 20;
		D.hp = 75;

		E.name = "Pusrv";
		E.power = 20;
		E.hp = 100;

		// upgrade 2
		F.name = "Morwe";
		F.power = 25;
		F.hp = 150;

		// upgrade 3
		G.name = "Rivp";
		G.power = 30;
		G.hp = 150;

		// upgrade 4
		H.name = "Fanara";
		H.power = 30;
		H.hp = 200;

		// upgrade 5
		I.name = "Bmace";
		I.power = 30;
		I.hp = 250;

		J.name = "Iloveyou";
		J.power = 35;
		J.hp = 250;
	}
}