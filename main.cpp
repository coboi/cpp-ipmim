#include <iostream>
#include <string>
#include <fstream>
#include "global.hpp"
#include "enemy.hpp"
#include "battle.hpp"
#include "question.hpp"
#include "upgrade.hpp"
#include "story.hpp"
#include "save_data.hpp"

void emptyHere(){
	std::cout << " Di sini gak ada apa-apa" << std::endl;
	var::meter += 1;
}

void soFar(){
	std::cout << " Kamu berjalan sejauh " << var::meter << " meter" << std::endl << std::endl;
}

int main(){
	save::isUpgraded();
	while(true){
		if(var::isUpgraded == "y"){
			save::restoreData();
			battle::upgradedStat();
		} else{
			save::restoreData();
			battle::defaultStat();
			var::tempPlayerMaxHp = battle::Player.getHp();
		}

		std::cout << "/";
		std::cin >> var::cmd;

		if(var::cmd == "lanjut"){
			if(var::meter == 0){
				story::setStory();
				save::saveData();
			} else if(var::meter == 1){
				soFar();
				std::cout << " Sekarang kamu bisa mukulin musuh" << std::endl;
				var::meter += 1;
				save::saveData();
			} else if(var::meter == 2){
				soFar();
				std::cout << " Dipukul balik sama musuh? jangan takut," << std::endl;
				std::cout << " sekarang kamu juga bisa bertahan" << std::endl << std::endl;
				std::cout << " Ketik info untuk lihat statusmu, atau" << std::endl;
				std::cout << " ketik keluar untuk keluar dari game" << std::endl;
				var::meter += 1;
				save::saveData();
			} else if (var::meter == 3){
				soFar();
				battle::startBattle();
				save::saveData();
			} else if(var::meter == 4){
				soFar();
				emptyHere();
				save::saveData();
			} else if(var::meter == 5){
				soFar();
				qn::questionTime();
				save::saveData();
				save::saveData();
			} else if(var::meter == 6){
				soFar();
				battle::startBattle();
				save::saveData();
			} else if(var::meter == 7){
				soFar();
				qn::questionTime();
				save::saveData();
			} else if(var::meter == 8){
				soFar();
				qn::questionTime();
				save::saveData();
			} else if(var::meter == 9){
				soFar();
				story::setStory();
				save::saveData();
			} else if(var::meter == 10){
				soFar();
				emptyHere();
				save::saveData();
			} else if(var::meter == 11){
				soFar();
				qn::questionTime();
				save::saveData();
			} else if(var::meter == 12){
				soFar();
				qn::questionTime();
				save::saveData();
			} else if(var::meter == 13){
				soFar();
				battle::startBattle();
				save::saveData();
			} else if(var::meter == 14){
				soFar();
				qn::questionTime();
				save::saveData();
			} else if(var::meter == 15){
				soFar();
				upgrade::selectUpgrade();
				save::saveData();
			} else if(var::meter == 16){
				soFar();
				emptyHere();
				save::saveData();
			} else if(var::meter == 17){
				soFar();
				emptyHere();
				save::saveData();
			} else if(var::meter == 18){
				soFar();
				qn::questionTime();
				save::saveData();
			} else if(var::meter == 19){
				soFar();
				battle::startBattle();
				save::saveData();
			} else if(var::meter == 20){
				soFar();
				battle::startBattle();
				save::saveData();
			} else if(var::meter == 21){
				soFar();
				story::setStory();
				save::saveData();
			} else if(var::meter == 22){
				soFar();
				qn::questionTime();
				save::saveData();
			} else if(var::meter == 23){
				soFar();
				emptyHere();
				save::saveData();
			} else if(var::meter == 24){
				soFar();
				qn::questionTime();
				save::saveData();
			} else if(var::meter == 25){
				soFar();
				upgrade::selectUpgrade();
				save::saveData();
			} else if(var::meter == 26){
				soFar();
				battle::startBattle();
				save::saveData();
			} else if(var::meter == 27){
				soFar();
				qn::questionTime();
				save::saveData();
			} else if(var::meter == 28){
				soFar();
				qn::questionTime();
				save::saveData();
			} else if(var::meter == 29){
				soFar();
				qn::questionTime();
				save::saveData();
			} else if(var::meter == 30){
				soFar();
				upgrade::selectUpgrade();
				save::saveData();
			} else if(var::meter == 31){
				soFar();
				emptyHere();
				save::saveData();
			} else if(var::meter == 32){
				soFar();
				qn::questionTime();
				save::saveData();
			} else if(var::meter == 33){
				soFar();
				emptyHere();
				save::saveData();
			} else if(var::meter == 34){
				soFar();
				story::setStory();
				save::saveData();
			} else if(var::meter == 35){
				soFar();
				battle::startBattle();
				save::saveData();
			} else if(var::meter == 36){
				soFar();
				upgrade::selectUpgrade();
				save::saveData();
			} else if(var::meter == 37){
				soFar();
				emptyHere();
				save::saveData();
			} else if(var::meter == 38){
				soFar();
				emptyHere();
				save::saveData();
			} else if(var::meter == 39){
				soFar();
				emptyHere();
				save::saveData();
			} else if(var::meter == 40){
				soFar();
				story::setStory();
				save::saveData();
			} else if(var::meter == 41){
				soFar();
				battle::startBattle();
				save::saveData();
			} else if(var::meter == 42){
				soFar();
				qn::questionTime();
				save::saveData();
			} else if(var::meter == 43){
				soFar();
				upgrade::selectUpgrade();
				save::saveData();
			} else if(var::meter == 44){
				soFar();
				qn::questionTime();
				save::saveData();
			} else if(var::meter == 45){
				soFar();
				battle::startBattle();
				save::saveData();
			} else if(var::meter == 46){
				soFar();
				qn::questionTime();
				save::saveData();
			} else if(var::meter == 47){
				soFar();
				emptyHere();
				save::saveData();
			} else if(var::meter == 48){
				soFar();
				battle::startBattle();
				save::saveData();
			} else if(var::meter == 49){
				story::setStory();
				save::saveData();
				break;
			}
		} else if(var::cmd == "info"){
			std::cout << " Hit Points (HP)  : " << battle::Player.getHp() << std::endl;
			std::cout << " Attack Power (AP): " << battle::Player.getPower() << std::endl << std::endl;
			std::cout << " Ternyata kamu kuat juga yaa" << std::endl;
		} else if(var::cmd == "keluar"){
			break;
		} else{
			std::cout << " Nulis apasih, gajelas" << std::endl;
		}
	}
	return 0;
}