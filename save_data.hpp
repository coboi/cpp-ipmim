namespace save{
	void saveData(){
		var::myData.open("data.dat", std::ios::out);
		var::myData << var::isUpgraded << std::endl;
		var::myData << var::meter << std::endl;
		var::myData << battle::Player.getName() << std::endl;
		var::myData << battle::Player.getPower() << std::endl;
		var::myData << var::tempPlayerMaxHp;
		var::myData.close();
	}

	void restoreData(){
		var::myData.open("data.dat", std::ios::in);
		var::myData >> var::isUpgraded;
		var::myData >> var::meter;
		var::myData >> var::tempPlayerName;
		var::myData >> var::tempPlayerPower;
		var::myData >> var::tempPlayerMaxHp;
		var::myData.close();

		battle::Player.setName(var::tempPlayerName);
		battle::Player.setPower(var::tempPlayerPower);
		battle::Player.setHp(var::tempPlayerMaxHp);
	}

	void isUpgraded(){
		var::myData.open("data.dat", std::ios::in);
		var::myData >> var::isUpgraded;
		var::myData.close();

		std::cout << "Ketik lanjut untuk mulai main ^^" << std::endl;
	}
}